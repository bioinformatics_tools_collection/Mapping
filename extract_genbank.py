#!/usr/bin/python3

def extract_name(infile):
    definition="UNKNOWN"
    accession="UNKNOWN"
    lines = 0
    for line in infile:
        if line.startswith("DEFINITION"):
            definition = " ".join(line.split(" ")[1:]).strip().replace(" ", "_")
        if line.startswith("ACCESSION"):
            accession = " ".join(line.split(" ")[1:]).strip().replace(" ", "_")
        lines += 1
        if accession != "UNKNOWN" and definition != "UNKNOWN":
            break
        if lines > 4:
            break
    return accession + " " + definition

def extract_sequence(infile):
    res = []
    numline = 0
    for line in infile:
        if line.startswith("ORIGIN"):
            break
    for line in infile:
        if line[0] == "/":
            seq = "".join(res)
            return "".join(seq)
        res += line.strip().split(" ")[1:]
        numline += 1

def write_sequence(outfile, seq, name, splitlen):
    f = open(outfile, "w")
    f.write(">" + name + "\n")
    pos = 0
    numline = 0
    while pos < len(seq):
        f.write(seq[pos:pos+splitlen])
        pos += splitlen
        f.write("\n")
        numline += 1

def main(infile, fastafile):
    #f = open(infile, "r")
    #linelength = 80
    #name = extract_name(f)
    #seq = extract_sequence(f)
    #f.close()
    #write_sequence(fastafile, seq, name, linelength)
    #return fastafile
    from Bio import SeqIO
    with open(infile, 'r') as f, open(fastafile, 'w') as g:
        sequences = SeqIO.parse(f, "genbank")
        for record in sequences:
            #x = record.id
            #record.id = x.split('.')[0]
            if record.name is not None:
                record.id = record.name
            SeqIO.write(record, g, "fasta")
    return fastafile
