# Mapping Pipeline
Version: 4.1.0 <br>
Contact: BI-Support@rki.de <br>
25.07.2017

---
## Introduction

This pipeline is used for mapping Next Generation Sequencing (NGS) reads of one or more samples automatically to a reference sequence (or a set thereof). Both raw Illumina read folders and trimmed read folders from [QCumber](https://gitlab.com/RKIBioinformaticsPipelines/QualityControl) can be used as input for the batch processing mode. 
The pipeline was implemented in Python (requiring Python 3.4 or higher to run) and uses the following tools:

| Tool name | Version |  Pubmed ID | 
|-----------|---------|----------|
| [Bowtie2*](http://bowtie-bio.sourceforge.net/bowtie2/index.shtml) | 2.3  |  [22388286](https://www.ncbi.nlm.nih.gov/pubmed/22388286) |
| [BWA*](http://bio-bwa.sourceforge.net/) | 0.7.15 | [20080505](https://www.ncbi.nlm.nih.gov/pubmed/20080505)  |
| [Latex](https://www.latex-project.org/) | 2.6 |
| [Samtools](http://www.htslib.org/)| 1.3.1 |[19505943](https://www.ncbi.nlm.nih.gov/pubmed/19505943) |
| RefFinder | 2.1.2 | [Stephan Fuchs, FG13](https://portal.rki.local/cocoon/portal/portallink?doctype=Navknoten&id=1340) |
| [Picard](https://broadinstitute.github.io/picard/)  | 2.7.1 | |
*At least one of the mapping softwares is needed.

The pipeline requires the following Python packages:
* jinja2
* matplotlib
* scipy
* numpy
* pyyaml
* pysam

The workflow of the pipeline is shown in the figure below:

 ![Workflow of the mapping pipeline.](./mapping_flow.png)

---
## Tutorial 
This tutorial works with trimmed reads generated in the QCumber tutorial. The files are available on sebio04 or sebio05 in the folder ``` /pipelines/datasets/QCResults ```. The outcome of QCumber contains four `"fastq.gz"` files per sample. The prefixes  ``` 1P ``` and ``` 2P ``` denote the first and second reads of read pairs, respectively. In case one of the read pairs did not survive trimming, its “orphaned” reads are contained in the files starting with ```1U ``` (read 1) or ```2U``` (read 2), respectively. A reference sequence is available at ```pipelines/datasets/references/toy_reference.gb ```.

To obtain a list of all parameters, the pipeline can be invoked with the command `mapping-4 -h`. 

The following command can be used to run the mapping pipeline on the test dataset:

```
mapping-4 --mapper bowtie2 --input /pipelines/datasets/QCResults/ --output mapping_outcome/ --reference /pipelines/datasets/references/toy_reference.gb --join_sample
```
Please note that the command needs to be entered in a single line, without line breaks. The arguments in the above command have the following meaning:
* ```--mapper bowtie2```: The name of the mapper of choice (the alternative is bwa-mem).
* ```--input```: The input folder containing the read files.
* ```--output ```: The output folder.
* ```--options```: Parameters passed directly to the mapper (bowtie2 in this case).
* ```--join_sample```: Join the alignment files for each sample. There are several files per sample from the QCumber output. These are mapped separately, which generates several output sam files per sample. This option requires the mapping pipeline to join all sam files generated for each sample into a single file.
* ```--reference```: The reference sequence to map against.


Once the pipeline has finished its computations, you can find the output in the mapper\_output folder. In case of ```--join_sample``` option, one PDF report ("mapping\_UP\_<samplename>.pdf") and one sam mapping file ("UP\_<samplename>.sam") will be generated for each sample. In absence of the  ```--join_sample``` argument, separated sample files for paired reads (```P```), unpaired read 1 (```1U```) and unpaired read 2 (```2U```) will be generated, along with the corresponding PDF reports.


Below please find some further usage examples:

```
mapping-4 --mapper bwa-mem --reference <path>/reference1.fasta --reference <path>/reference2.gb --read1 <path>/read1.fastq --read2 <path>/read2.fastq --output <path> --options "-M"
```

```
mapping-4 --mapper bwa-mem --read1 <path>/read1.fastq --read2 <path>/read2.fastq --option "-x /index/bwa"
```

```
mapping-4 --mapper bowtie2 --input <path>/to/project --reference <path>/to/reference --join <path/to/picard>
```

---
## Options
The following paragraphs describe the options of the pipeline in detail.

#### Input
> Long option: `--input <INPUT>` <br>
> Short option: `-i <INPUT>`
 
Input mapping files. The input can be:
* a fastq file containing the forward reads given by the `--read1` parameter. The corresponding fastq file containing reverse reads can be given using the `--read2` parameter
* a folder containing raw MiSeq data (fastq.gz files named following the default MiSeq naming scheme) given by the `--input` parameter
* the "Trimmed" output folder of QCumber given by `--input` parameter

#### Read1
> Long option: `--read1 <READ1>` <br>
> Short option: `-r1 <READ1>`
 
Either unpaired reads or first mate of paired reads. Alternatively, select a project folder using `--input`.

#### Reference
> Long option: `--reference <REFERENCE>` <br>
> Short option: `-r <REFERENCE>`
 
Reference genome. FASTA or GenBank format. This parameter can be used multiple times to provide multiple sequences (please refer to the option `multi-fasta` for further information.

GenBank files may be acquired from [NCBI](http://www.ncbi.nlm.nih.gov/genbank/). When downloading GenBank files from NCBI, make sure to include the sequence (*Customize view* -> *Display options* -> *Show sequence*) before downloading the file (*Send* -> *Complete record* -> *File* -> *GenBank (full)*). Alternatively, reference sequence can be exported as GenBank file from **Geneious**.

#### Help
> Long option: `--help ` <br>
> Short option: `-h`
 
Show the help messages and exit

#### Version
> Long option: `--version ` <br>
> Short option: `-v `
 
Show program's version number and exit

#### No-report
> Long option: `--no-report `

Do not generate report. Default: generate a report.


#### Read2
> Long option: `--read2 <READ2>` <br>
> Short option: `-r2 <READ2>`
 
Second mate of paired reads.

#### Output
> Long option: `--output <OUTPUT>` <br>
> Short option: `-o <OUTPUT>`
 
Output location. Default: current working directory `./`

#### Threads
> Long option: `--threads <THREADS>` <br>
> Short option: `-t <THREADS>`
 
Define number of threads used by mapper. Default: 1

#### Log
> Long option: `--log <LOG>` <br>
> Short option: `-l <LOG>`
 
Define log file. Default: log file with timestamp in output directory

#### Multi-fasta
> Long option: `--multi-fasta`

Per default, multiple sequences of a given reference FASTA file are considered to be different genetic segments (e.g., different chromosomes). In case `--multi-fasta` is used, the pipeline splits the multi-FASTA file into single FASTA files, and subsequently calls RefFinder (developed by Dr. Stephan Fuchs) to automatically select the best reference.

#### Options
> Long option: `--options <OPTIONS>`
 
The parameter `--options` can be used to provide further parameters that will be passed directly to the mapper. Please quote the whole parameter string (e.g. `--options "--very-sensitive-local"` to use the very sensitive local alignment with bowtie2, please refer to the man pages of `bowtie2` and bwa for respective parameter lists).

#### Reffinder options
> Long option: `--reffinder_options <REFFINDER_OPTIONS>`

In case multiple references are given as separate FASTA files, this option can be used to provide parameters to "RefFinder" (developed by Dr. Stephan Fuchs) for the determination of the best reference. The parameter string has to be quoted, e.g.: ` --reffinder_options=″-t 10″ `.

#### Join
> Long option: `--join `
 
Join all created alignment files into one file.  Default: one SAM file per fastq file.

#### Join sample
> Long option: `--join_sample `
 
Join all created alignment files per sample into one file. Default: one SAM file per fastq file.

#### No-rg
> Long option: `--no-rg `
 
Do not set read groups. Default: set distinct read group for each sample.

#### Unmapped
> Long option: `--unmapped <UNMAPPED>`
 
Write unmapped reads to output. Read name will be automatically added. Default: unmapped reads are not outputted.

#### Java
> Long option: `--java <JAVA>`

Path to the Java version 1.8+ executable.

#### Samtools
> Long option: `--samtools <SAMTOOLS>`
 
Path to folder containing samtools.

#### Bwa
> Long option: `--bwa <BWA>`
 
Path to folder containg bwa.

#### Bowtie2
> Long option: `--bowtie2 <BOWTIE2>`
 
Path to folder containing bowtie2.

#### Picard
> Long option: `--picard <PICARD>`
 
Path to folder containing picard.jar

#### Config
> Long option: `--config <CONFIG>` <br>
> Short option: `-c <CONFIG>`
 
Define a config file in yaml format with preset parameters. If both config file and command parameters are given, the parameters given on the commandline supersede the parameters given in the config file. Thus, a config file can be used to personalize default parameters, and deviations from these personal defaults can be given via the command-line.

For instance, in order to run the pipeline with 24 threads using `"~/SCRATCH_NOBAK/runs/newest_run"` as input, `"~/SCRATCH_NOBAK/runs/newest_mappingres"` as output and `"~/references/myreference.gb"` as reference, the following config file could be used (`"~/example.yaml"`):

```
threads: 24
input: "~/SCRATCH_NOBAK/runs/newest_run"
output: "~/SCRATCH_NOBAK/runs/newest_mappingres"
reference: "~/references/myreference.gb"
```

The pipeline could then be started using the following command:

```
mapping-4 --config ~/example.yaml
```

If then a a run of the pipeline is to be started with the same parameters but with 12 threads, this can be achieved with the following command (re-using the config file and overriding the threads option instead of giving all paramenters on the commandline manually):

```
mapping-4 --config ~/example.yaml --threads 12
```

---
#### Output description
The output folder contains:

* The mapping file in BAM format
* The mapping files (SAM)
* PDF reports, as well as the auxiliary tex files 
* log files are located in the output folder. 
* A directory `data`, which contains subfolders for each sample holding the graphs (PDF) and `json` config files.

Along with the sam files resulting from the mapping procedure, PDF report(s) containing different mapping metrics is generated. The report also includes general information about the run, e.g. start time, executor and options. The first page of each PDF report contains an overview of all tool versions used in the pipeline, the parameters passed to the pipeline and the system characteristics. The second page summarizes several mapping metrics. The figure below shows a representative second page of the mapping report. The four figures show the following information:
* Top left: The maximum, average and minimum coverage over the length of the reference sequence.
* Top right: The number of bases in the reference covered by different number of reads. The presence of distinct peaks in this graph may indicate potentially interesting events such as copy number variations.
* Bottom left: The number of mapped reads vs. mapping qualities. Low mapping quality of majority of reads may indicate unsuitable reference sequence(s).
* Bottom right: The maximum, average and minimum mapping quality over the length of the reference sequence.

 ![Example of the PDF report generated by the mapping pipeline.](report.png)

Moreover, some additional metrics are documented in the PDF report:
* total reads
* concordant reads
* discordant reads
* paired reads
* mapped reads
* unmapped reads
* coverage graph
* bases per coverage graph
* reads per mapping quality graph
* mapping quality graph
* average coverage
* maximum coverage
* minimum coverage
* standard deviation of coverage
* number of bases with coverage of 0
* number of bases with coverage > mean+2*sd
* number of bases with coverage < mean+2*sd

---
## Changelog 
 
 * 4.1.0: Reworked readgroup assignment 
 * 4.0.0: Adaptions for wrapper pipeline
 * 3.0.0: Individual Read Group Lib per sample
 * 2.0.2: Parameter names are now unified across all RKI Bioinformatics Service Pipelines
 * 2.0.1: Changed executable name
 * 2.0: Initial release of the stable version
 * 1.0: Initial version for internal testing

---
## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License, version 3 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program.  If not, see http://www.gnu.org/licenses/.

