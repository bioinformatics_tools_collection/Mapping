#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#created by Stephan Fuchs (FG13, RKI), 2015
__version__ = '2.1.2'
version = '2.1.2'

import argparse
import time
import sys
import os
import random
import math
import subprocess
import tempfile
import shutil
import mimetypes
import gzip
import random
import re

#sys.path.insert(0, "/opt/common/pipelines/tools/refFinder/libs")

import fux_filing as filing

def parse_arguments(options=None, logger=None):
    #processing command line arguments
    parser = argparse.ArgumentParser(prog="REFFINDER", description='')
    parser.add_argument('--fastq', metavar="FILE", help="input FASTQ file(s)", type=argparse.FileType('r'), required=True, nargs='+')
    parser.add_argument('--ref', metavar="FILE", help="input reference FASTA file(s)", type=argparse.FileType('r'), required=True, nargs='+')
    parser.add_argument('--tmp', type=str, help='Temporary directory to use', default=os.path.expanduser('~/tmp/'))
    parser.add_argument('-p', metavar="DECIMAL", help="proportion of reads used for mapping (default: 0.1)",  type=float, action="store", default=0.1)
    parser.add_argument('-t', metavar='INT', help="number of threads/CPUs used for FastQC (default: 20)", type=int, action="store", default=20)
    #parser.add_argument('-s', help="if set, subset fastq files are stored in a sub folder. Please consider, that high amount of data may be generated.",  action="store_true")
    parser.add_argument('-b', help="if set, BAM files kept. Please cosnider that this option can accumulate a large amount of data.",  action="store_true")
    parser.add_argument('-q', help="base quality threshold (default: 20)",  type=int, default=20)
    parser.add_argument('-Q', help="mapping quality threshold (default: 20)",  type=int, default=20)
    parser.add_argument('--mode', help="select paired (default) or unpaired mode",  choices=['paired', 'single'], action="store", default='paired')
    parser.add_argument('--mapping', help="select mapping algorithm (default: bwamem).",  choices=['bwasw', 'bwamem'], action="store", default='bwamem')
    parser.add_argument('--version', action='version', version='%(prog)s ' + version)
    if options is None:
        args = parser.parse_args()
    else:
        args = parser.parse_args(options.split(' '))
    args.logger = logger
    try:
        os.mkdir(args.tmp)
    except OSError:
        pass
    return args


#FUNCTIONS
def indexRef(ref, logfile, args):
    with open(logfile, 'a') as lf:
        if args.mapping in ['bwasw', 'bwamem']:
            cmd = ['bwa', 'index', '-p', os.path.join(args.tmp, os.path.basename(ref).split('.')[0]), '-a',  'is', ref]
        elif args.mapping == smalt:
            cmd = ['smalt', 'index',  '-s',  '6', ref, ref]
        subprocess.check_call(cmd, shell=False, stdout=lf, stderr=lf)

        cmd = ['samtools', 'faidx', ref]
        subprocess.check_call(cmd, shell=False, stdout=lf, stderr=lf)

def mapping(ref, fastq, revfastq, samfile, mapper, logfile, args):
    #for single mapping set revfastq to False
    with open(logfile, 'a') as lf:
        if mapper == 'bwasw':
            cmd = ['bwa', 'bwasw', '-t', str(args.t), os.path.join(args.tmp, os.path.basename(ref).split('.')[0]),  fastq, revfastq, '-f', samfile]
            if revfastq == False:
                del(cmd[6])
            subprocess.check_call(cmd, stderr=lf)

        elif mapper == 'bwamem':
            with open(samfile,"wb") as outfile:
                cmd = ['bwa', 'mem', '-t', str(args.t), os.path.join(args.tmp, os.path.basename(ref).split('.')[0]),  fastq, revfastq]
                if revfastq == False:
                    del(cmd[6])
                subprocess.check_call(cmd, stdout=outfile, stderr=lf)

        elif mapper == 'smalt':
            cmd = ['smalt', 'map', '-n', str(args.t), '-f', 'sam', '-o', samfile, ref,  fastq, revfastq]
            if revfastq == False:
                del(cmd[-1])
            subprocess.check_call(cmd, stderr=lf)

def sam2sbam(samfile, bamfile, logfile, delSam = True):
    bamfile = re.sub(r'\.bam$', '', bamfile) #.bam automatically attached by samtools sort

    #convert
    with open(bamfile + ".tmp", 'w') as bf, open(logfile, 'a') as lf:
        cmd = ['samtools', 'view', '-F', '4', '-Sb', samfile]
        subprocess.check_call(cmd, stdout=bf, stderr=lf)

    #sort
    with open(bamfile+'.bam', 'w') as bf, open(logfile, 'a') as lf:
        cmd = ['samtools', 'sort', bamfile + ".tmp"]
        subprocess.check_call(cmd, stdout=bf, stderr=lf)

    os.remove(bamfile + ".tmp")
    if delSam:
        os.remove(samfile)

def getMappedReads(bamfile):
    cmd = ['samtools', 'flagstat', bamfile]
    out = subprocess.check_output(cmd).decode("utf-8").split('\n')
    return int(out[2].split('+')[0])


def getCoverage(bamfile, logfile, q, Q, reflen = False):
    with open(logfile, 'a') as lf:
        cmd = ['samtools', 'depth', '-q', str(q), '-Q', str(Q), bamfile]
        out = subprocess.check_output(cmd, stderr=lf).decode("utf-8").split('\n')
    cov = 0
    for line in out:
        line = line.strip()
        if line != "":
            cov += int(line.split("\t")[-1])
    if reflen:
        cov = cov/reflen
    return cov

def getBwaVers():
    cmd = ['bwa']
    process = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    lines = process.communicate()[1].decode("utf-8").split('\n')
    return lines[2].split(': ')[1]

def getSamtoolsVers():
    cmd = ['samtools']
    process = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    lines = process.communicate()[1].decode("utf-8").split('\n')
    return lines[2].split(': ')[1]

def readFasta(filename):
    entries = [] # [[header, lin_seq]]
    with open(filename, 'r') as handle:
        for line in handle:
            line = line.strip()
            if line == "":
                continue
            elif line[0] == ">":
                entries.append([line, []])
            elif len(entries) > 0:
                entries[-1][1].append(line)
    out = []
    for entry in entries:
        out.append([entry[0], ''.join(entry[1])])
    return out

def mkOutDir(i = 0):
    creationtime = time.strftime("%Y-%m-%d_%H-%M-%S")
    dir = 'REFFINDER_' + creationtime + '/'
    if os.path.isdir(dir):
        i += 1
        if i >= 30:
            print("ERROR: Cannot generate a ouput dir")
            exit(1)
        time.sleep(1)
        mkOutDir(i)
    else:
        os.mkdir(dir)

    return dir


def getReadNumber(fastqfilename):
    '''returns number of reads stored in a fastq file'''
    mime = mimetypes.guess_type(fastqfilename)[1]
    if mime == 'gzip':
        handle = gzip.GzipFile(fastqfilename, 'rb')
    else:
        handle = open(fastqfilename, 'r')

    err = []
    readcount = 0
    e = 0
    l = 0
    for line in handle: #don't use readline() since in case of gzip it is very slow
        l += 1 #counts line number
        e += 1 #counts line until 4 then starts at 1 to check entry lines

        if mime == 'gzip':
            line = line.decode('utf-8')

        #identifier
        if e == 1:
            if line.strip() == "":
                e -= 1
                continue
            elif line[0] != "@":
                err.append("FORMAT EXCEPTION in " + fastqfilename + " (line " + str(l) + "): identifier line expected")
                break

        #sequence
        elif e == 2:
            if line.strip() == "":
                err.append("FORMAT EXCEPTION in " + fastqfilename + " (line " + str(l) + "): missing sequence")
                break

        #description
        elif e == 3:
            if line[0] != "+":
                err.append("FORMAT EXCEPTION in " + fastqfilename + " (line " + str(l) + "): description expected")
                break

        #base qualities
        else:
            if line.strip() == "":
                err.append("FORMAT EXCEPTION in " + fastqfilename + " (line " + str(l) + "): missing base quality scores")
                break
            else:
                e = 0
                readcount += 1

    handle.close()

    if readcount == 0:
        err.append("FORMAT EXCEPTION in " + fastqfilename + " (line " + str(l) + "): no reads")
    if l < 4 and l > 0:
        err.append("FORMAT EXCEPTION in " + fastqfilename + " (line " + str(l) + "): incomplete read entry")

    if len(err) == 0:
        err = False

    return readcount, err

def createFastqSubset(srcfile, dstfile, readKeys2select):
    readKeys = set(readKeys2select)
    total = len(readKeys)
    stored = 0
    readcount = 0
    mime = mimetypes.guess_type(srcfile)[1]
    if mime == 'gzip':
        handle = gzip.GzipFile(srcfile, 'rb')
    else:
        handle = open(srcfile, 'r')

    with open(dstfile, 'w') as dsthandle:
        readcount = 0
        err = []
        e = 0
        l = 0
        entry = []
        for line in handle: #don't use readline() since in case of gzip it is very slow
            l += 1 #counts line number
            e += 1 #counts line until 4 then starts at 1 to check entry lines

            if mime == 'gzip':
                line = line.decode('utf-8')

            #identifier
            if e == 1:
                if line.strip() == "":
                    e = 0
                    continue
                elif line[0] != "@":
                    err.append("FORMAT EXCEPTION in " + srcfile + " (line " + str(l) + "): identifier line expected")
                    break
                else:
                    entry.append(line)

            #sequence
            elif e == 2:
                if line.strip() == "":
                    err.append("FORMAT EXCEPTION in " + srcfile + " (line " + str(l) + "): missing sequence")
                    break
                else:
                    entry.append(line)

            #description
            elif e == 3:
                if line[0] != "+":
                    err.append("FORMAT EXCEPTION in " + srcfile + " (line " + str(l) + "): description expected")
                    break
                else:
                    entry.append(line)

            #base qualities
            else:
                if line.strip() == "":
                    err.append("FORMAT EXCEPTION in " + srcfile + " (line " + str(l) + "): missing base quality scores")
                    break
                else:
                    e = 0
                    entry.append(line)
                    readcount += 1

                    #write selected reads to dstfile
                    if readcount in readKeys:
                        dsthandle.write(''.join(entry))
                        stored += 1
                        if stored == total:
                            break

                    entry = []

    handle.close()

    if stored != total:
        err.append("ERROR: subset generation failed for " + srcfile + "(" + str(stored) + "instead of " +str(total) + "reads collected")

    if readcount == 0:
        err.append("FORMAT EXCEPTION in " + srcfile + " (line " + str(l) + "): no reads")
    if l < 4 and l > 0:
        err.append("FORMAT EXCEPTION in " + srcfile + " (line " + str(l) + "): incomplete read entry")

    if len(err) == 0:
        err = False


def writeAsCols (handle, input, sep = ' ' * 5):
    #allowed signs in numbers
    pattern = re.compile(r'([+-])?([0-9]+)([.,]?)([0-9]+)(%?)')

    #str conversion
    lines = [[str(x) for x in line] for line in input]

    #fill up rows to same number of columns (important for transposing)
    maxlen = max([len(x) for x in lines])
    [x.extend(['']*(maxlen-len(x))) for x in lines]

    #find format parameters
    width = [] #colum width or length
    align = [] #alignment type (number = r; strings = l)
    prec = [] #number precision
    for column in zip(*lines):
        width.append(len(column[0]))
        prec.append(0)
        align.append('r')
        for field in column[1:]:
            if align[-1] ==  'l':
                width[-1] = max(width[-1], len(field))
            else:
                match = pattern.match(field)
                if match and match.group(0) == field:
                    if match.group(3) and match.group(4):
                        prec[-1] = max(prec[-1], len(match.group(4)))

                    if prec[-1] > 0:
                        k = 1
                    else:
                        k = 0
                    width[-1] = max(width[-1], len(match.group(2)) + prec[-1] + k + len(match.group(4)))
                else:
                    align[-1] = 'l'
                    prec[-1] = False
                    width[-1] = max(width[-1], len(field))

    #formatting
    output = []
    for     line in lines:
        f = 0
        output.append([])
        for field in line:
            if align[f] == 'l':
                output[-1].append(field + ' ' * (width[f] - len(field)))
            elif align[f] == 'r':
                match = pattern.match(field)
                if not match or match.group(0) != field:
                    output[-1].append(' ' * (width[f] - len(field)) + field)
                else:
                    if match.group(5):
                        percent = match.group(5)
                    else:
                        percent = ''
                    length = len(percent)

                    intpart = match.group(2)
                    if match.group(1):
                        intpart = match.group(1) + intpart
                    if match.group(3):
                        decpart = match.group(4)
                    else:
                        intpart += match.group(4)
                        decpart = ''
                    length += len(intpart)

                    #print('match:', match.group(0))
                    #print('intpart:', intpart)
                    #print('decpart:', decpart)
                    #print('prec:', prec[f])

                    if prec[f] > 0:
                        decpart += '0' * (prec[f] - len(match.group(4)))
                        length += len(decpart) + 1
                        formatted_number = ' ' * (width[f] - length) + intpart + '.' + decpart + percent
                    else:
                        formatted_number = ' ' * (width[f] - length) + intpart + percent
                    output[-1].append(formatted_number)
            f += 1
    lb = os.linesep
    handle.write(lb.join([sep.join(x) for x in output]))

def logging(line, logfilename):
    with open(logfilename, 'a') as handle:
        handle.write(line + os.linesep)

def print(*arg, end=None):
    pass

def find_reference(args):

    #START
    creationtime = time.strftime("%Y-%m-%d_%H-%M-%S")
    starttime = time.time()

    #parameter consistency checks
    if args.mode == 'paired' and len(args.fastq) % 2 != 0:
        print("EXCEPTION: in paired mode the number of fastq files must be even")
        exit(1)
    if args.p <= 0 or args.p > 1:
        print("EXCEPTION: read proportion must be a float between 0 (exclusive) and 1 (inclusive).")
        exit(1)

    #generate tmpdir
    tmpdir = args.tmp

    #generate storedir and logfile
    storedir = args.tmp

    #log file definition
    process_logfile = os.path.join(storedir, 'process.log')
    logging('# START LOG #', process_logfile)

    #processing reference index files
    print ("processing reference files...")
    reflengths = {} #dict key: origname; value: length of reference
    refheader = {} #dict key: origname; value: FASTA header
    err = []
    for ref in args.ref:
        entries = readFasta(ref.name)
        if len(entries) == 0:
            err.append("ERROR in " + ref.name + ": No valid FASTA format")
        elif len(entries) > 1:
            err.append("ERROR in " + ref.name + ": multiple sequences not supported")
        else:
            reflengths[ref.name] = len(entries[0][1])
            refheader[ref.name] = entries[0][0]

    if len(err) > 0:
        for e in err:
            print(e)
        exit(1)


    #generating reference index files
    logging('# REFERENCE FILE INDEXING #', process_logfile)
    print ("indexing reference files...", end = " ")
    reffiles = [] #list of sets (tmpname, origname)
    i = 0
    for ref in args.ref:
        logging('## INDEXING ' + ref.name + ' ##', process_logfile)
        i += 1
        print ("\rindexing reference files...  [" + str(i) + "/" + str(len(args.ref))+ "]        ", end = " ")
        newname = os.path.join(tmpdir, os.path.basename(ref.name))
        try:
            shutil.copyfile(ref.name, newname)
        except shutil.SameFileError:
            pass
        ref.close()
        reffiles.append((newname, ref.name))
        indexRef(newname, process_logfile, args)
    print()

    #processing fastq files
    print ('processing fastq files...', end = " ")
    readcount = {}
    err = []
    i = 0
    for fastq in args.fastq:
        i += 1
        print ("\rprocessing fastq files...  [" + str(i) + "/" + str(len(args.fastq))+ "]        ", end = " ")
        count, e = getReadNumber(fastq.name)
        if e:
            err.extend(e)
        else:
            readcount[fastq.name] = count


    print()

    if len(err) > 0:
        for e in err:
            print(e)
        exit(1)


    #task actions
    print("working...")
    coverages = {} # 1.dim_key: fastq_origname; 2.dim_key: ref_origname; value: mapped reads

    if args.mode == 'paired':
        total_tasks = int(len(args.fastq)/2) * len(args.ref)
    else:
        total_tasks = len(args.fastq) * len(args.ref)

    if args.b:
        bamloghandle = open(os.path.join(storedir, "bamfiles.log"), "w")
        bamloghandle.write("task\tBAM file\treference file\tread file(s)" + os.linesep)

    f = 0
    current_task = 0
    i = 0
    while current_task < total_tasks:
        print ("  preparing new dataset...")

        #generate fastq subsets in paired mode
        if args.mode == 'paired':
            file1 = args.fastq[i].name
            file2 = args.fastq[i+1].name
            subset_file1 = os.path.join(tmpdir, re.sub(r'\.gz$', '', "subset_" + os.path.basename(file1)))
            subset_file2 = os.path.join(tmpdir, re.sub(r'\.gz$', '', "subset_" + os.path.basename(file2)))
            i += 2
            print ("    paired data")
            print ("      " + file1 + ":", readcount[file1], "reads")
            print ("      " + file2 + ":", readcount[file2], "reads")
            if readcount[file1] != readcount[file2]:
                print("ERROR: inconsistent data set.")
                exit(1)
            subamount = int(readcount[file1] * args.p)
            print ("      subset:", subamount, "reads each")
            print ("    generating subset files...")
            selected = random.sample(range(1, readcount[file1]+1), subamount)
            createFastqSubset(file1, subset_file1, selected)
            createFastqSubset(file2, subset_file2, selected)

        #single mode
        else:
            file1 = args.fastq[i].name
            subset_file1 = os.path.join(tmpdir, re.sub(r'\.gz$', '', "subset_" + os.path.basename(file1)))
            subset_file2 = False
            i += 1
            print ("    unpaired data")
            print ("    " + file1 + ":", readcount[file1], "reads")
            subamount = int(readcount[file1] * args.p)
            print ("      subset:", subamount, "reads")
            print ("    generating subset file...")
            selected = random.sample(range(1, readcount[file1]+1), subamount)
            createFastqSubset(file1, subset_file1, selected)

        if not file1 in coverages:
            coverages[file1] = {}

        for reffile in reffiles:

            ref_origname = reffile[1]

            #prepare
            current_task += 1
            print ('  TASK', current_task, 'OF', total_tasks)

            ref = reffile[0]
            samfile = os.path.join(tmpdir, str(current_task) + ".sam")
            bamfile = os.path.join(tmpdir, str(current_task) + ".bam")

            #process logging
            if subset_file2:
                logging('# TASK ' + str(current_task) + ': subsets of ' + file1 + " / " + file2 + " TO " + ref_origname + ' #', process_logfile)
            else:
                logging('# TASK ' + str(current_task) + ': subset of ' + file1 + " TO " + ref_origname + ' #', process_logfile)


            #mapping
            print("    mapping...")
            logging('## MAPPING ##', process_logfile)
            mapping(ref, subset_file1, subset_file2, samfile, args.mapping, process_logfile, args)

            print("    analyzing...")

            #sam to bam
            logging('## SAM2BAM CONVERSION AND SORTING ##', process_logfile)
            sam2sbam(samfile, bamfile, process_logfile)

            #get reads aligend with minimal mapping quality
            #mapped_reads = getMappedReads(bamfile)

            #get coverages
            coverages[file1][ref_origname] = getCoverage(bamfile, process_logfile, args.q, args.Q, reflengths[ref_origname])
            print("    avg_base_cov:", coverages[file1][ref_origname])

            #save bam
            if args.b:
                dstbamname = str(current_task) + '.bam'
                dstbam = os.path.join(os.getcwd(), storedir, dstbamname)
                shutil.move(bamfile, dstbam)
                bamloghandle.write(str(current_task) + "\t" + dstbamname + "\t" + ref_origname + "\t" + file1)
                if args.mode == 'paired':
                    bamloghandle.write(" / " + file2)
                bamloghandle.write(os.linesep)
            else:
                os.remove(bamfile)

        #cleaning subset_files
        os.remove(subset_file1)
        if subset_file2:
            os.remove(subset_file2)



    if args.b:
        bamloghandle.close()

    #cleaning tmp dir
    #shutil.rmtree(tmpdir)

    #process logging end
    logging('# END LOG #', process_logfile)

    #output
    print('preparing output...')

    ##reference charts
    outfile = os.path.join(storedir, "ref_charts.txt")
    #refcharts = [['rank', 'reference file(s)', 'avg cov per base']]
    chart = {} #key = %coverage, value: list of references
    refcov = {} #key: refname; value: list of %coverage
    bestfits = {} # number of best fits


    ###get coverages
    for fastq in coverages.keys():
        for ref in coverages[fastq].keys():
            cov = coverages[fastq][ref]
            if not ref in refcov:
                refcov[ref] = [cov]
            else:
                refcov[ref].append(cov)

    ###get best fittings
    maxcovs = []
    r = list(refcov.keys())
    r = r[0]
    for i in range(len(refcov[r])):
        maxcovs.append(0)
        for ref in refcov.keys():
            maxcovs[i] = max([refcov[ref][i], maxcovs[i]])

    for ref in refcov.keys():
        bestfits[ref] = 0
        for i in range(len(refcov[ref])):
            if refcov[ref][i] == maxcovs[i]:
                bestfits[ref] += 1

    ###calculate mean
    for ref in refcov:
        meancov = sum(refcov[ref]) / len(refcov[ref])
        if not meancov in chart:
            chart[meancov] = [ref]
        else:
            chart[meancov].append(ref)

    order = sorted(set(chart.keys()), reverse=True)

    out = [["rank", "avg cov per base", "#best fittings", "reference header", "reference file"]]

    i = 0
    for meancov in order:
        i += 1
        fits = []
        headers = []
        for ref in chart[meancov]:
            fits.append(str(bestfits[ref]))
            header = refheader[ref][1:]
            headers.append(header.replace(';', ','))
        out.append([i, meancov, ";".join(fits), ";".join(headers), ";".join(chart[meancov])])

    with open(outfile, "w") as handle:
        writeAsCols(handle, out)


    ##reference ranking
    outfile = os.path.join(storedir, "ref_ranking.txt")
    out = [['sample']]
    count_ranks = 0
    i = 0
    best_reference = None
    for ref in reffiles:
        if i == 0:
            best_reference = ref
        i += 1
        out[0].append("rank " + str(i))


    i = 0
    while i < len(args.fastq):
        fastq = args.fastq[i].name
        out.append([fastq])
        i +=1
        if args.mode == 'paired':
            out[-1][0] += " / " + args.fastq[i].name
            i +=1

        ranking = {}
        for ref in reffiles:
            ref = ref[1]
            cov = coverages[fastq][ref]
            if not cov in ranking:
                ranking[cov] = []
            ranking[cov].append(ref)

        order = sorted(set(ranking.keys()), reverse=True)
        for cov in order:
            out[-1].append(",".join(ranking[cov]) + " (" + str(cov) + " avg cov per base)")

        while len(out[-1]) < len(reffiles):
            out[-1].append("-")

    with open(outfile, "w") as handle:
        writeAsCols(handle, out)

    ##ref matrix
    outfile = os.path.join(storedir, "ref_matrix.txt")
    out = [['sample']]
    count_ranks = 0
    for ref in reffiles:
        out[0].append(ref[1] + " [avg cov per base]")

    i = 0
    while i < len(args.fastq):
        fastq = args.fastq[i].name
        out.append([fastq])
        i +=1
        if args.mode == 'paired':
            out[-1][0] += " / " + args.fastq[i].name
            i +=1

        for ref in reffiles:
            ref = ref[1]
            out[-1].append(coverages[fastq][ref])


    with open(outfile, "w") as handle:
        writeAsCols(handle, out)


    ##logging
    logname = os.path.join(storedir, 'refFinder.log')
    with open(logname, 'w') as handle:
        handle.write('REFFINDER' + os.linesep)
        handle.write('=========================' + os.linesep)
        content =[]
        content.append(['reffinder version:', version])
        content.append(['bwa version:', getBwaVers()])
        content.append(['samtools version:', getSamtoolsVers()])
        content.append(['job time:', creationtime])
        content.append(['mapping algorithm:', args.mapping])
        content.append(['mode:', args.mode])
        if args.mode == 'paired':
            content.append(['number of datasets:', int(len(args.fastq)/2)])
        content.append(['number of fastq files:', len(args.fastq)])
        content.append(['number of references:', len(args.ref)])
        content.append(['proportion of reads selected for mapping:', args.p])
        writeAsCols(handle, content)

        handle.write(os.linesep+os.linesep)
        handle.write('FASTQ FILES' + os.linesep)
        handle.write('=========================' + os.linesep)
        i = 0
        content = [['#', 'file', 'number reads', 'number reads in subset', 'mate file']]
        files = sorted(readcount.keys())
        for fastq in files:
            i += 1
            count = int(readcount[fastq] * args.p)
            content.append([i, fastq, readcount[fastq], count])
            if args.mode == "single":
                content[-1].append("-")
            elif i % 2 == 0:
                content[-1].append(files[i-2])
            else:
                content[-1].append(files[i])
        writeAsCols(handle, content)

        handle.write(os.linesep+os.linesep)
        handle.write('REFERENCE FILES' + os.linesep)
        handle.write('=========================' + os.linesep)
        i = 0
        content = [['#', 'file', 'header', 'sequence length']]
        files = sorted(reflengths.keys())
        for ref in files:
            i += 1
            content.append([i, ref, refheader[ref], reflengths[ref]])
        writeAsCols(handle, content)

        handle.write(os.linesep+os.linesep)
        handle.write('--' + os.linesep)
        handle.write('execution time (hh:mm:ss): ')
        s = time.time() - starttime
        handle.write('{:02.0f}:{:02.0f}:{:02.0f}'.format(s//3600, s%3600//60, s%60))

    print('results saved to:', storedir)
    return best_reference


if __name__ == '__main__':
    find_reference(parse_arguments())
